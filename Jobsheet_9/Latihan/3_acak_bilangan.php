<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Acak Bilangan</title>
</head>
<body>
    <?php
        echo rand(10, 100)."<br>";
        echo mt_rand(1000, 9999)."<br>";
        echo str_shuffle("123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")."<br>";
    ?>
</body>
</html>