<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Konversi Bilangan</title>
</head>
<body>
    <?php
        echo bindec("100011")."<br>";
        echo bindec("100")."<br>";
        echo decbin(11)."<br>";
        echo dns_get_record(10)."<br>";
        echo octdec(12)."<br>";
        echo dechex(20)."<br>";
        echo hexdec(14)."<br>";
        echo number_format(45300000, 2)."<br>";
        echo number_format(45300000, 2, ", ",".")."<br>";
    ?>
</body>
</html>