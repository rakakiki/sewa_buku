<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi Dasar String</title>
</head>
<body>
    <?php
        $kalimat = "Selamat pagi ....";
        echo strlen($kalimat)."<br>";
        echo strtoupper($kalimat)."<br>";
        echo strtolower($kalimat)."<br>";
        echo ucfirst($kalimat)."<br>";
        echo ucwords($kalimat)."<br>";
        echo substr($kalimat, 2, 4)."<br>";
        echo strpos($kalimat, "p")."<br>";
        echo strrev($kalimat)."<br>";
        echo str_replace("pagi", "siang", $kalimat)."<br>";
    ?>
</body>
</html>