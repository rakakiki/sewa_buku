<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ekspresi Reguler</title>
</head>
<body>
    <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"])?>" method="post">
        Email : <input type="text" name="email"><br>
        <input type="submit" value="Cek">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $email = $_POST['email'];
            if(empty($_POST['email'])) {
                echo "email harus diisi";
            } else {
                $ekspresi = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
                
                if(preg_match($ekspresi, $email))
                    echo "Alamat email <b>".$email."</b> valid";
                else
                    echo "Alamat email <b>".$email."</b> tidak valid";
            }
        }
    ?>
</body>
</html>