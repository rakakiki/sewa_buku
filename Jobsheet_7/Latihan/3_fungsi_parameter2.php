<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi dengan Parameter 2</title>
</head>
<body>
    <?php
        echo "Fungsi dengan parameter untuk penjumlahan <br>";
        function fungsiTambah($angka1, $angka2) {
            $jumlah = $angka1 + $angka2;
            echo "Jumlah dari dua angka : $jumlah";
        }
        fungsiTambah(12, 20);
        echo "<br> <br>";
        
        echo "Fungsi dengan parameter untuk string <br>";
        function biodata($nama, $tahun) {
            echo "$nama lahir pada tahun $tahun <br>";
        }
        biodata("Gilang", 2004);
        biodata("Elang", 2003);
    ?>
</body>
</html>