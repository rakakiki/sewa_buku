<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi Parameter</title>
</head>
<body>
    <?php
        function perkenalan ($nama, $salam) {
            echo $salam."<br>";
            echo "Perkenalkan nama saya ".$nama."<br>";
            echo "Senang berkenalan dengan anda! <br>";
        }
        perkenalan("Gilang", "Hi");
        echo "<hr>";
        $saya = "Gilang Raka Ramadhan";
        $ucapSalam = "Selamat Pagi!";
        perkenalan($saya, $ucapSalam);
    ?>
</body>
</html>