<?php
    $operator = ["+", "-", "*", "/"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- 
        Nama : Gilang Raka Ramadhan
        NIM  : 3.34.22.2.08
        Kelas: IK-2C

        Tugas: Buatlah sebuah program sederhana menggunakan operator aritmatika dengan melibatkan 3 variabel!
     -->
     <form action="" method="post">
        <fieldset style="width:650px; margin:auto;">
            <legend>Program Kalkulator 3 Variabel</legend>
            <input type="number" name="bilangan1" id="bilangan1" placeholder="Bilangan Pertama">
            <select name="operator1" id="operator1">
                <?php
                    foreach($operator as $key => $value):
                        echo '<option value="'.$key.'">'.$value.'</option>';
					endforeach;
				?>	
            </select>
            <input type="number" name="bilangan2" id="bilangan2" placeholder="Bilangan Kedua">
            <select name="operator2" id="operator2">
                <?php
                    foreach($operator as $key => $value):
                        echo '<option value="'.$key.'">'.$value.'</option>';
					endforeach;
				?>
            </select>
            <input type="number" name="bilangan3" id="bilangan3" placeholder="Bilangan Ketiga">
            <input type="submit" name="hitung" value="Hitung">
        </fieldset>
     </form>

    <?php
        if(isset($_POST['hitung'])) {
            $bilangan1 = $_POST['bilangan1'];
            $bilangan2 = $_POST['bilangan2'];
            $bilangan3 = $_POST['bilangan3'];
            //Cari Hasil 1
            switch ($_POST['operator1']) {
                case array_search('+', $operator):
                    $hasil1 = $bilangan1 + $bilangan2; 
                    break;
                case array_search('-', $operator):
                    $hasil1 = $bilangan1 - $bilangan2; 
                    break;
                case array_search('*', $operator):
                    $hasil1 = $bilangan1 * $bilangan2; 
                    break;
                case array_search('/', $operator):
                    $hasil1 = $bilangan1 / $bilangan2; 
                    break;
                default:
                    echo "Keyword Salah";
                    break;
            }
            //Cari Hasil 2
            switch ($_POST['operator2']) {
                case array_search('+', $operator):
                    $hasil2 = $hasil1 + $bilangan3; 
                    break;
                case array_search('-', $operator):
                    $hasil2 = $hasil1 - $bilangan3; 
                    break;
                case array_search('*', $operator):
                    $hasil2 = $hasil1 * $bilangan3; 
                    break;
                case array_search('/', $operator):
                    $hasil2 = $hasil1 / $bilangan3; 
                    break;
                default:
                    echo "Keyword Salah";
                    break;
            }

            echo "
                <p style='text-align: center; color: red;'>Hasil : ".$hasil2."</p>
            ";
        }
    ?>

</body>
</html>
