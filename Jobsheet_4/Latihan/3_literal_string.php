<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Literal String</title>
</head>
<body>
    <?php
        $nama = "Gilang";
        print("Selamat pagi, $nama<br>");
        print('Selamat pagi, $nama<br>');
        print("Selamat pagi, \"$nama\"<br>");
        print("Selamat pagi \\$nama<br>");
        print("Selamat pagi \$$nama<br>");

        // Gini juga bisa
        print("Halo, {$nama}");
    ?>
</body>
</html>