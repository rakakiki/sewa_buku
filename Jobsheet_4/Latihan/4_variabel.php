<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Variabel</title>
</head>
<body>
    <?php
        $teks = "Selamat Pagi!";
        $x = 8;
        $y = 13.7;

        echo "{$teks}";
        echo "<br>";
        echo "{$x}";
        echo "<br>";
        echo "{$y}";
    ?>
</body>
</html>