<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operator Penugasan</title>
</head>
<body>
    <?php
        $x = 100;
        echo $x; echo "<br>";
        $x += 5;
        echo $x; echo "<br>";
        $x -= 5;
        echo $x; echo "<br>";
        $x /= 2;
        echo $x; echo "<br>";
        $x %= 35;
        echo $x; echo "<br>";
        $x ^= 2;
        echo $x; echo "<br>";
    ?>
</body>
</html>