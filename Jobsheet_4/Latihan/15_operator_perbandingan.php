<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operator Perbandingan</title>
</head>
<body>
    <?php
        $x = 40;
        $y = "40";
        var_dump($x == $y); echo "<br>";
        var_dump($x === $y); echo "<br>";
        var_dump($x != $y); echo "<br>";
        var_dump($x <> $y); echo "<br>";
        var_dump($x !== $y); echo "<br>";
        var_dump($x > $y); echo "<br>";
        var_dump($x < $y); echo "<br>";
        var_dump($x >= $y); echo "<br>";
        var_dump($x <= $y); echo "<br>";
        echo ($x <=> $y);
    ?>
</body>
</html>