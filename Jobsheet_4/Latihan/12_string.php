<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String</title>
</head>
<body>
    <?php
        echo strlen("Selamat Pagi");
        echo "<br>";

        echo str_word_count("Selamat Pagi");
        echo "<br>";

        echo str_replace("Gilang", "Raka", "Nama saya Gilang");
        echo "<br>";

        echo substr("Nama saya Gilang", 2, 6);
        echo "<br>";

        echo strtolower("HALO, SAYA GILANG");
        echo "<br>";

        echo strtoupper("halo, saya gilang");
        echo "<br>";

        echo ucfirst("awal paragraf");
        echo "<br>";

        echo ucwords("teknik informatika polines");
        echo "<br>";
    ?>
</body>
</html>