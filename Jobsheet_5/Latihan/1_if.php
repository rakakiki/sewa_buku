<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kondisi If</title>
</head>
<body>
    <?php
        $mood = "Senang";
        if ($mood == "Senang") {
            echo "Hari ini mood saya baik";
        }
    ?>
</body>
</html>