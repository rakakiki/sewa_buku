<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kondisi If Else</title>
</head>
<body>
    <?php
        $suhu = 20;

        //Kondisi menggunakan If Else
        if ($suhu <= 20) {
            echo "Suhu Sejuk";
        } elseif ($suhu > 20 and $suhu <= 27) {
            echo "Suhu Biasa";
        } else {
            echo "Udara Panas";
        }
    ?>
</body>
</html>