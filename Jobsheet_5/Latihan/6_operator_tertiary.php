<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operator Tertiary</title>
</head>
<body>
    <?php
        $a = 15;
        $b = 30;
        $a > $b ? $c = $a : $c = $b;
        echo "Nilai terbesar : $c";
    ?>
</body>
</html>