<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Switch Case</title>
</head>
<body>
    <?php
        $buah = "apel";
        switch ($buah) {
            case "mangga" :
                echo "Buah is apple";
            break;
            case "jeruk" :
                echo "Buah is bar";
            break;
            case "anggur" :
                echo "Buah is cake";
            break;
            default :
                echo "Buah favorit anda tidak tersedia";
        }
    ?>
</body>
</html>