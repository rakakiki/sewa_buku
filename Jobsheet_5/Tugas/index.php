<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Program Bilangan Prima</title>
</head>
<body>
    <!-- 
        Nama : Gilang Raka Ramadhan
        NIM  : 3.34.22.2.08
        Kelas: IK-2C

        Tugas: Buatlah program kecil menampilkan deret bilangan prima!
     -->
    <form action="" method="post">
        <fieldset style="width: 430px; margin: auto;">
            <legend>Program Menampilkan Deret Bilangan Prima</legend>

            <input type="number" name="bilangan1" id="bilangan1" placeholder="Masukkan Bilangan Awal">
            <input type="number" name="bilangan2" id="bilangan2" placeholder="Masukkan Bilangan Akhir">

            <input type="submit" name="tampilkan" value="Tampilkan">
        </fieldset>
    </form>

    <?php
        if (isset($_POST['tampilkan'])) {
            $bil_awal = $_POST['bilangan1'];
            $bil_akhir = $_POST['bilangan2'];

            for ($i = $bil_awal; $i <= $bil_akhir; $i++) {
                if ($i <= 1) {
                    continue;
                }
                if (($i==2 or $i==3 or $i==5 or $i==7) or ($i%2 != 0 and $i%3 != 0 and $i%5 != 0 and $i%7 != 0)) {
                    echo $i."<br>";
                }
                continue;
            }
        }
    ?>
</body>
</html>