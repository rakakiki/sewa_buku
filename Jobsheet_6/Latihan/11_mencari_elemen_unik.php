<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mencari Elemen Unik</title>
</head>
<body>
    <?php
        $nilai = [80, 90, 80, 67, 90, 87, 75];
        $elemen_unik = array_unique($nilai);

        while (list($indeks, $elemen) = each($elemen_unik)) {
            echo $elemen."<br>";
        }
        
        // Error saat dijalankan, karena fungsi each sudah tidak ada di PHP 7++
    ?>
</body>
</html>