<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menampilkan Array</title>
</head>
<body>
    <?php
        $minuman_fav = ["Teh", "Kopi", "Es Cokelat"];
        
        for ($i = 0; $i < 3; $i++) {
            echo $minuman_fav[$i]; 
            echo "<br>";
        }

        // Bisa Juga Pakai ini
        foreach ($minuman_fav as $i) {
            echo $i;
            echo "<br>";
        }
    ?>
</body>
</html>