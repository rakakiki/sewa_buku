<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array Multidimensi</title>
</head>
<body>
    <?php
        $mahasiswa = array (
            'id' => array(1, 2, 3, 4),
            'nama' => array("Gilang", "Elang", "Anda", "Figor"),
            'prodi' => array("IK", "Telkom", "IK", "Elektronika")
        );
    ?>
    
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Program Studi</th>
        </tr>

        <?php
            for($i=0; $i<count($mahasiswa)+1; $i++) {
        ?>
            <tr>
                <td><?= $mahasiswa['id'][$i] ?></td>
                <td><?= $mahasiswa['nama'][$i] ?></td>
                <td><?= $mahasiswa['prodi'][$i] ?></td>
            </tr>
        <?php
            }
        ?>
    </table>
</body>
</html>