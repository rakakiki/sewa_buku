<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cara Menulis Array</title>
</head>
<body>
    <?php
        $hobi = array("rebahan", "main gitar", "membaca");
        $minuman_fav = ["Teh", "Kopi", "Es Cokelat"];
        print_r($hobi);
        echo "<br>";
        print_r($minuman_fav);
    ?>
</body>
</html>