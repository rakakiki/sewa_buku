<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mengurutkan Array</title>
</head>
<body>
    <?php
        $minuman_fav = ["Teh", "Kopi", "Es Cokelat"];
        $panjang_arr = count($minuman_fav);

        //Mengurutkan Dari Depan (Asc)
        sort($minuman_fav);
        for($i=0; $i<$panjang_arr; $i++) {
            echo $minuman_fav[$i]."<br>";
        } 
        echo "<br>";

        //Mengurutkan Dari Belakang (Desc)
        rsort($minuman_fav);
        for($i=0; $i<$panjang_arr; $i++) {
            echo $minuman_fav[$i]."<br>";
        } 
        echo "<br>";
    ?>
</body>
</html>