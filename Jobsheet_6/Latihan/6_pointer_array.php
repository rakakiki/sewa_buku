<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pointter Array</title>
</head>
<body>
    <?php
        $minuman_fav = ["Teh", "Kopi", "Es Cokelat", "Susu"];
        
        $pointer_current = current($minuman_fav);
        $pointer_end = end($minuman_fav);
        $pointer_prev = prev($minuman_fav);
        $pointer_reset = reset($minuman_fav);
        $pointer_next = next($minuman_fav);

        echo "Elemen Pertama Array adalah ".$pointer_current."<br>";
        echo "Elemen Terakhir Array adalah ".$pointer_end."<br>";
        echo "Elemen Array sebelum Array Terakhir adalah ".$pointer_prev."<br>";
        echo "Reset Array sehingga Kembali ke Elemen Pertama ".$pointer_reset."<br>";
        echo "Elemen Array Berikutnya adalah ".$pointer_next."<br>";
    ?>
</body>
</html>