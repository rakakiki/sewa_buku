<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menambah dan Mengurangi Array</title>
</head>
<body>
    <?php
        $minuman_fav = ["Teh", "Kopi", "Es Cokelat"];

        //Menghapus Elemen Terakhir
        $elemen_terakhir = array_pop($minuman_fav);
        echo "Elemen yang dihapus : ".$elemen_terakhir."<br>";

        //Menambah Elemen
        $tambah_el = array_push($minuman_fav, 'Teh Gelas');
        echo "Array setelah ditambah elemen baru : <br>";
        
        for($i = 0; $i < count($minuman_fav); $i++) {
            echo $minuman_fav[$i]."<br>";
        }
        //Menghapus Elemen Pertama
        $elemen_pertama = array_shift($minuman_fav);
        echo "Elemen yang dihapus : ".$elemen_pertama."<br>";
    ?>
</body>
</html>