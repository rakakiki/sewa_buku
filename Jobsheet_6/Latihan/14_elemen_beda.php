<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Elemen Beda</title>
</head> 
<body>
    <?php
        $nilai_kelasA = array(78, 90, 95, 80, 97, 60);
        $nilai_kelasB = array(90, 80, 75, 76, 85, 76);

        $el_beda = array_diff($nilai_kelasA, $nilai_kelasB);
        print_r($el_beda);

        foreach($el_beda as $i) {
            echo $i."<br>";
        }
    ?>
</body>
</html>