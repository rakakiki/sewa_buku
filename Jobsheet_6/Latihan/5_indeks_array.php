<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Indeks Array</title>
</head>
<body>
    <?php
        //Array dengan indeks string
        $bunga['satu'] = "Mawar";
        $bunga['dua'] = "Lilac";
        $bunga['tiga'] = "Anggrek";

        echo $bunga['satu'];
        echo "<br>";

        //Array dengan index campuran
        $warna['red'] = "Merah";
        $warna[7] = "Kuning";
        $warna['purple'] = "Ungu";

        echo $warna['red'];
        echo "<br>";
    ?>
</body>
</html>