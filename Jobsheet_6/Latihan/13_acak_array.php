<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Acak Array</title>
</head>
<body>
    <?php
        $minuman_fav = ["Cappucino", "Es Teh", "Kopi", "Es Buah"];
        $acak_arr = shuffle($minuman_fav);

        foreach ($minuman_fav as $i) {
            echo $i."<br>";
        }
    ?>
</body>
</html>