<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>15 Fungsi Array</title>
</head>
<body>
    <!-- 
        Nama : Gilang Raka Ramadhan
        NIM  : 3.34.22.2.08
        Kelas: IK-2C

        Tugas: Buatlah Program dengan menggunakan fungsi array minimal 15 fungsi
     -->

     <?php
        // Membuat Array
        $mahasiswa = array('Gilang', 'Elang', 'Yufigor', 'Anda');

        // 1. Menampilkan Array dengan ForEach() (Belum ada di Latihan)
        foreach ($mahasiswa as $i) {
            echo $i."<br>";
        }
        echo "<br>";
        
        // 2. array_combine() untuk menggabungkan 2 array menjadi 1 array (arg pertama sebagai keys, arg kedua sebagai values)
        $mahasiswa_nilai = array(80, 85, 90, 95);
        $combine = array_combine($mahasiswa, $mahasiswa_nilai);
        print_r($combine)."<br>";
     ?>
        <table border="1">
            <tr>
                <th>Nama</th>
                <th>Nilai</th>
            </tr>
    <?php
        foreach ($combine as $key => $value) {
    ?>
            <tr>
                <td><?= $key ?></td>
                <td><?= $value ?></td>
            </tr>
    <?php
        }
    ?>
        </table>
    <?php
        echo "<br>";
        // 3. array_merge() untuk menggabungkan 2 array
        $merge = array_merge($mahasiswa, $mahasiswa_nilai);
        foreach($merge as $i) {
            echo $i."<br>";
        }
        echo "<br>";

        // 4. in_array(), fungsi untuk mencari data pada array. Apabila data tersedia, maka akan bernilai 1. Jika tidak tersedia, maka akan bernilai 0.
        $cari = in_array("Gilang", $mahasiswa);
        echo $cari."<br>";

        echo "<br>";

        // 5. array_slice() untuk memotong array
        $arr = [0,1,2,3,4,5,6,7,8,9,0];
        $arr_baru = array_slice($arr, 2, 8);
        print_r($arr_baru);

        echo "<br>";
        echo "<br>";

        // 6. array_reverse() untuk membalik array sesuai indeks
        $buah = array('Apel', 'Timun', 'Pisang');
        print_r(array_reverse($buah));  //Jadi Pisang, Timun, Apel

        echo "<br>";
        echo "<br>";

        // 7. array_search() untuk mencari key dari array dengan menginputkan value
        $warna = array('a'=>'red', 'b'=>'blue', 'c'=>'yellow');
        print_r(array_search('red', $warna)); //Return a

        echo "<br>";
        echo "<br>";

        // 8. array_keys() untuk menampilkan keys dari array
        $mhs = array('Gilang'=>80, 'Galang'=>100, 'Hendrik'=>75);
        $mhs_key = array_keys($mhs);
        foreach($mhs_key as $i) {
            echo $i."<br>";
        } //Return Gilang, Galang, Hendrik

        echo "<br>";
        echo "<br>";

        // 9. array_values() untuk menampilkan value dai array
        $mhs_val = array_values($mhs);
        foreach($mhs_val as $i) {
            echo $i."<br>";
        } //Return 80, 100, 75

        echo "<br>";
        echo "<br>";

        // 10. array_splice() untuk meremove elemen array dan mengganti dengan elemen lain
        $arr1 = array('Red', 'Green', 'Blue', 'Purple');
        $arr2 = array('Magenta', 'Cyan');
        array_splice($arr1, 0, 2, $arr2);
        print_r($arr1);

        echo "<br>";
        echo "<br>";

        // 11. list(), fungsi untuk memecah elemen dalam array menjadi variabel-variabel
        $nama_mhs = array('Gilang', 'Galang', 'Ramadhan');
        list($a, $b, $c) = $nama_mhs;
        echo "Mahasiswa Polines : $a, $b, dan $c.";
        
        echo "<br>";
        echo "<br>";

        // 12. range() untuk membuat array berisi nomor dari range yang diinput
        $nomor = range(1, 10); //Membuat array nomor, kemudian mengisinya dengan 1 sampai 10
        foreach($nomor as $i) {
            echo $i." ";
        }

        echo "<br>";
        echo "<br>";

        // 13. array_replace() untuk mengubah isi dari array pertama menjadi array kedua
        $arr1 = array('id1' => 'Adit', 'id2' => 'Sopo', 'id3' => 'Jarwo');
        $arr2 = array('id1' => 'Upin', 'id2' => 'Ipin');
        $arr3 = array_replace($arr1, $arr2);
        foreach($arr3 as $i) {
            echo $i." ";
        }

        echo "<br>";
        echo "<br>";

        // 14. array_sum() untuk menjumlahkan nilai didalam array. Contoh penggunaannya adalah rata2 nilai kelas
        $nilai_kelas = array(80, 85, 90, 95, 100);
        $average = array_sum($nilai_kelas) / sizeof($nilai_kelas);
        echo $average;

        echo "<br>";
        echo "<br>";

        // 15. array_product() untuk mengkalikan nilai di dalam array
        $persegi_panjang = array('panjang' => 10, 'lebar' => 2);
        $luas_persegi_panjang = array_product($persegi_panjang);
        echo $luas_persegi_panjang;
    ?>
</body>
</html>